# All stack rust
Since Rust support is increasing for both embedded and web assembly targets, it start becomes possible to implement the whole code stack from embedded to the website in the same language.
This repo and the submodules contains a proof of concept in which the possibility of running Rust on all layers of a stack from embedded to web are explored.

The project consists of a client website, which is flashed on to a stm32f407 discovery board, which provides a webserver, and ADC for data aggregation and a websocket for communication.

# Clone
Clone all necessary subrepos with:
```bash
git clone --recursive git@gitlab.com:all-stack-rust/all-stack-rust.git
```

# Components
## Client
The client website is written with the yew framework. 
It allows the user to open the websocket connection and then receives the ADC data, calculates the furrier transform and plots it. 
Since the used stm32f407 discovery board doesn't have a SD-Card slot, the website will be flashed directly together with the firmware.
To compile the website, install wasm-pack first:
```bash
cargo install wasm-pack 
```
and then run:
```bash
cd client
wasm-pack build --target web --out-name wasm --out-dir ./static
```

## Communication
The communication submodule contains definitions for a shared communication protocol with serde based on postcard.

## Firmware
As hardware, a stm32f407 discovery board is used. 
The firmware is fully static, uses an webserver and a websocket to server the client website and an ADC to generate data for the FFT.
### Build and flash
To compile and flash the code install cargo flash and the matching toolchain.
```bash
rustup target add thumbv7em-none-eabihf
cargo install cargo-flash
cargo install cargo-embed
```
Once installed, flash and build it with:
```bash
cd firmware
cargo build
cargo flash --chip STM32F407VGTx
```
Or flash with:
```bash
cargo-embed
```

For some reason, the connection with cargo flash might timeout. 
It can help to rerun the command. If that doesn't help, cargo-embed seems to work more stable.

### Communication
Since the board does not have a ethernet port, the data is sent over USART2, and on the host computer translated to a TCP connection.
This is done with the ser2net tool.
```bash
ser2net -d -t 2 -C "4242:raw:600:/dev/PATH:115200 8DATABITS NONE 1STOPBIT LOCAL -RTSCTS max-connections=3"
```
Once the command ist run, a TCP port on 4242 is opened and the website can now be loaded by opening http://localhost:4242.
Loading it will take around 30 seconds. The progress can be tracked on the networking page in the dev tools of the browser. 
